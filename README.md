# ETF Fond Admin

This project is for learning Scala with Play and TypeScript with React.

ETF Fond Admin is a tool to import the ETF and Fonds of the DKB.
The data is in the beginning a PDF-file, which will be converted to a JSON file.

## Setup of the project

I used the setup-example from [Yohan Gomez](https://twitter.com/yohangz).
For more information visit the following links:

- https://github.com/yohangz/scala-play-react-seed
- https://blog.usejournal.com/react-with-play-framework-2-6-x-a6e15c0b7bd